import { CHANGE_TAB_ACTIVE, CHUYEN_TAB, DAT_VE, DAT_VE_HOAN_TAT } from "./../constants/ticketsConstants";
let initialState = {
  danhSachGheDangDat: [],
  tabActive: '1' 
};
export const ticketsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case DAT_VE: {
      let cloneDanhSachGheDangDat = [...state.danhSachGheDangDat];
      
      let index = cloneDanhSachGheDangDat.findIndex(
        (gheDD) => gheDD.maGhe === payload.maGhe
      );

      if (index !== -1) {
        cloneDanhSachGheDangDat.splice(index, 1);
      } else {
        cloneDanhSachGheDangDat.push(payload);
      }
      return { ...state, danhSachGheDangDat: cloneDanhSachGheDangDat };
    }
    case DAT_VE_HOAN_TAT:{
      state.danhSachGheDangDat =[];
      return {...state}
    }
    case CHANGE_TAB_ACTIVE:{
      state.tabActive = payload
      return {...state}
    }
    default:
      return state;
  }
};
