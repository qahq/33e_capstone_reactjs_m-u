import { SET_LOGIN, SET_REGISTER } from "./../constants/userConstants";
import { userInforLocal } from "./../../service/local.service";

let initialState = {
  //Khi user load trang
  userInfor: userInforLocal.get(),
};
export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_LOGIN: {
      state.userInfor = payload;
      return { ...state };
    }
    case SET_REGISTER: {
      state.userInfor = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
