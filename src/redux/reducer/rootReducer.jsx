import { combineReducers } from "redux";
import { userReducer } from './userReducer';
import { spinnerReducer } from './spinnerReducer';
import { ticketsReducer } from './ticketsReducer';



export const rootReducer  = combineReducers({userReducer,spinnerReducer,ticketsReducer})