import { DAT_VE_HOAN_TAT } from "redux/constants/ticketsConstants";
import { ThongTinDatVe } from "./../../models/thongTinDatVe";
import { movieService } from "./../../service/movie.service";
import { CHUYEN_TAB } from "./../constants/ticketsConstants";

export const datVeAction = (thongTinDatVe = new ThongTinDatVe()) => {
  return async (dispatch) => {
    try {
      let res = await movieService.setTickets(thongTinDatVe);
      await dispatch({
        type: DAT_VE_HOAN_TAT,
      });
    } catch (error) {}
  };
};
