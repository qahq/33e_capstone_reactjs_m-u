import { message } from "antd";
import { userInforLocal } from "./../../service/local.service";
import { userServ } from "./../../service/user.service";
import { SET_LOGIN, SET_REGISTER } from "./../constants/userConstants";
export const setLoginActionService = (values, onSuccess) => {
  return (dispatch) => {
    userServ
      .postLogin(values)
      .then((res) => {
        //Lưu thông tin đăng nhập xuống localStorage
        userInforLocal.set(res.data.content);
        dispatch({
          type: SET_LOGIN,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };
};
export const setRegisterActionService = (values, onSuccess) => {
  return (dispatch) => {
    userServ
      .postRegister(values)
      .then((res) => {
        dispatch({
          type: SET_REGISTER,
        });
        onSuccess();
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };
};
