import React, { Fragment, useEffect, useState } from "react";
import "./bookTicket.css";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { movieService } from "./../../service/movie.service";
import {
  CloseOutlined,
  FullscreenExitOutlined,
  UserOutlined,
  WindowsFilled,
} from "@ant-design/icons";
import {
  CHANGE_TAB_ACTIVE,
  DAT_VE,
} from "./../../redux/constants/ticketsConstants";
import { ThongTinDatVe } from "./../../models/thongTinDatVe";
import { datVeAction } from "./../../redux/actions/ticketsAction";
import Header from "components/Header/Header";
import { message } from "antd";
import { Tabs } from "antd";
import { userServ } from "./../../service/user.service";
import moment from "moment";
import _ from "lodash";

function BookticketPage(props) {
  let dispatch = useDispatch();
  let params = useParams();
  let { id } = params;

  const [dataTicket, setDataTicket] = useState([]);
  let fetchDataTicket = () => {
    movieService
      .getListTicketRoom({ MaLichChieu: id })
      .then((res) => {
        setDataTicket(res.data.content);
      })
      .catch((err) => {});
  };
  useEffect(() => {
    fetchDataTicket();
  }, []);
  let danhSachGheDangDat = useSelector((state) => {
    return state.ticketsReducer.danhSachGheDangDat;
  });

  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  let renderSeats = () => {
    return dataTicket.danhSachGhe?.map((ghe, index) => {
      let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : "";
      let classGheDaDat = ghe.daDat === true ? "gheDaDat" : "";
      let classGheDangDat = "";
      let indexGheDD = danhSachGheDangDat.findIndex(
        (gheDD) => gheDD.maGhe == ghe.maGhe
      );
      let classGheDaDuocDat = "";
      if (userInfor.taiKhoan === ghe.taiKhoanNguoiDat) {
        classGheDaDuocDat = "gheDaDuocDat";
      }

      if (indexGheDD !== -1) {
        classGheDangDat = "gheDangDat";
      }
      return (
        <Fragment key={index}>
          <button
            onClick={() => {
              dispatch({
                type: DAT_VE,
                payload: ghe,
              });
            }}
            disabled={ghe.daDat}
            className={`ghe ${classGheVip} ${classGheDaDat} ${classGheDangDat} ${classGheDaDuocDat} text-center`}
            key={index}
          >
            {ghe.daDat ? (
              classGheDaDuocDat != "" ? (
                <UserOutlined
                  style={{ marginBottom: 7.5, fontWeight: "bold" }}
                />
              ) : (
                <CloseOutlined
                  style={{ marginBottom: 7.5, fontWeight: "bold" }}
                />
              )
            ) : (
              ghe.stt
            )}
          </button>
          {(index + 1) % 16 === 0 ? <br /> : ""}
        </Fragment>
      );
    });
  };
  let { thongTinPhim } = dataTicket;
  return (
    <div>
      <div>
        <div className="grid grid-cols-12">
          <div className="col-span-9">
            <div className="flex flex-col items-center justify-center">
              <div>{renderSeats()}</div>
            </div>
            <div className="mt-5 flex justify-center">
              <table
                className="divide-y divide-gray-200 w-2/3"
                style={{ border: "none" }}
              >
                <thead className=" p-5">
                  <tr>
                    <th>Ghế chưa đặt</th>
                    <th>Ghế đang đặt</th>
                    <th>Ghế đã đặt</th>
                    <th>Ghế vip</th>
                    <th>Ghế mình đặt</th>
                  </tr>
                </thead>
                <tbody className=" divide-y divide-gray-200">
                  <tr>
                    <td>
                      <button className="ghe text-center">
                        <FullscreenExitOutlined
                          style={{ marginBottom: 7.5, fontWeight: "bold" }}
                        />
                      </button>
                    </td>
                    <td>
                      <button className="ghe gheDangDat text-center">
                        <FullscreenExitOutlined
                          style={{ marginBottom: 7.5, fontWeight: "bold" }}
                        />
                      </button>
                    </td>
                    <td>
                      <button className="ghe gheDaDat text-center">
                        <FullscreenExitOutlined
                          style={{ marginBottom: 7.5, fontWeight: "bold" }}
                        />
                      </button>
                    </td>
                    <td>
                      <button className="ghe gheVip text-center">
                        <FullscreenExitOutlined
                          style={{ marginBottom: 7.5, fontWeight: "bold" }}
                        />
                      </button>
                    </td>
                    <td>
                      <button className="ghe gheDaDuocDat text-center">
                        <FullscreenExitOutlined
                          style={{ marginBottom: 7.5, fontWeight: "bold" }}
                        />
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div className="col-span-3 text-left">
            <h3 className="text-yellow-500 text-center text-2xl font-bold my-2">THÔNG TIN VÉ</h3>
            <h2 className="text-yellow-500 text-center text-2xl font-bold my-2">
              Tổng tiền : {""}
              {danhSachGheDangDat
                .reduce((sum, ghe, index) => {
                  return (sum += ghe.giaVe);
                }, 0)
                .toLocaleString()}{" "}
              Đ
            </h2>
            <hr />
            <div className="text-white">
              <h3 className="text-xl text-white mt-2">Tên phim : {thongTinPhim?.tenPhim}</h3>
              <p> Cụm Rạp : {thongTinPhim?.tenCumRap} - </p>
              <p>Địa chỉ : {thongTinPhim?.diaChi}</p>
              <p>Rạp : {thongTinPhim?.tenRap} </p>
              <p className="my-2">
                Ngày giờ chiếu : {thongTinPhim?.ngayChieu} -{" "}
                {thongTinPhim?.gioChieu}{" "}
              </p>
            </div>
            <hr />
            <div className="grid grid-cols-2">
              <div className="p-2">
                <span className="text-orange-700 text-xl">Ghế : </span>
              </div>
              <div className="text-right text-green-700">
                {danhSachGheDangDat.map((gheDD, index) => {
                  return (
                    <span key={index} className="text-orange-600 text-xl">
                      Ghế {gheDD.stt},
                    </span>
                  );
                })}
              </div>
            </div>
            <hr />
            <div className="my-3 text-white">
            <i>Tên khách hàng  </i>
              <br />
              {userInfor.hoTen}
            </div>
            <div className="my-3 text-white">
              <i>Email </i>
              <br />
              {userInfor.email}
            </div>
            <div className="my-3 text-white">
              <i>Số điện thoại </i>
              <br />
              {userInfor.soDT}
            </div>
            <div
              className="flex flex-col items-center"
              
            >
              <div
                onClick={() => {
                  const thongTinDatVe = new ThongTinDatVe();
                  thongTinDatVe.maLichChieu = Number(id);
                  thongTinDatVe.danhSachVe = danhSachGheDangDat;

                  dispatch(datVeAction(thongTinDatVe));
                  message.success("Đặt vé thành công !");
                  fetchDataTicket();
                }}
                className="bg-orange-700 text-white rounded font-bold w-full text-center py-3 text-xl cursor-pointer"
              >
                ĐẶT VÉ
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default function (props) {
  let { tabActive } = useSelector((state) => state.ticketsReducer);
  let dispatch = useDispatch();
  return (
    <div>
      <Header />
      <div
        className="p-2" 
        style={{
          marginTop: 80,
          backgroundImage:
            "url(https://media.istockphoto.com/photos/empty-red-armchairs-of-a-theater-ready-for-a-show-picture-id1295114854?b=1&k=20&m=1295114854&s=170667a&w=0&h=W9ZbN674554Jsamxo5AfoO3DrSm_7qYS1EnANgusi9o=)",
          backgroundPosition: "center",
          backgroundSize: "cover",
          backgroundRepeat:"no-repeat",
          width: "100%",
          height: "100%",
          backgroundColor: "rgba(0,0,0,0.7)"
        }}
      >
        <Tabs
          defaultActiveKey="1"
          activeKey={tabActive}
          onChange={(key) => {
            dispatch({
              type: CHANGE_TAB_ACTIVE,
              payload: key,
            });
          }}
        >
          <Tabs.TabPane
            tab="01 - CHỌN GHẾ & THANH TOÁN"
            key="1"
           className={'ant-tabs-tab-btn'}
          >
            <BookticketPage />
          </Tabs.TabPane>
          <Tabs.TabPane tab="02 - LỊCH SỬ ĐẶT VÉ" key="2" className={'ant-tabs-tab-btn'}>
            <HistoryBooking {...props} />
          </Tabs.TabPane>
        </Tabs>
      </div>
    </div>
  );
}

function HistoryBooking(props) {
  const [dataUser, setDataUser] = useState([]);
  let fetchDataUser = () => {
    userServ
      .postThongTinNguoiDung()
      .then((res) => {
        setDataUser(res.data.content);
      })
      .catch((err) => {});
  };
  useEffect(() => {
    fetchDataUser()
  }, []);
  let renderTicketsItem = () => {
    return dataUser.thongTinDatVe?.map((ticket, index) => {
      let seats = _.first(ticket.danhSachGhe);
      return (
        <div key={index} className="p-2 lg:w-1/3 md:w-1/2 w-full">
          <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
            <img
              alt="team"
              className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4"
              src={ticket.hinhAnh}
            />
            <div className="flex-grow">
              <h1 className="text-white title-font font-medium  ">
                Tên phim : {ticket.tenPhim}
              </h1>
              <p className="text-white">
                Giờ chiếu : {moment(ticket.ngayDat).format("hh:mm A")} - Ngày
                chiếu : {moment(ticket.ngayDat).format("DD-MM-YYYY")}
              </p>
              <p className="text-white">
                Thời lượng phim : {ticket.thoiLuongPhim} phút
                <p className="text-white">Địa điểm : {seats.tenHeThongRap}</p>
              </p>
              <p className="text-white">
                {" "}
                Tên rạp : {seats.tenCumRap} - Ghế{" "}
                {ticket.danhSachGhe.map((ghe, index) => {
                  return <span key={index}>{ghe.tenGhe} </span>;
                })}
              </p>
            </div>
          </div>
        </div>
      );
    });
  };
  return (
    <div>
      <section className="text-white body-font pb-40  ">
        <div className="container px-5 py-5 mx-auto h-full w-full">
          <div className="flex flex-col text-center w-full mb-10">
            <h1 className="sm:text-3xl text-2xl font-medium title-font mb-2 text-white ">
              LỊCH SỬ ĐẶT VÉ KHÁCH HÀNG
            </h1>
            <p className="lg:w-2/3 mx-auto leading-relaxed text-white font-bold text-xl">
              Khách hàng xem lịch sử vé mình đã đặt tại đây !
            </p>
          </div>
          <div className="flex flex-wrap">{renderTicketsItem()}</div>
        </div>
      </section>
    </div>
  );
}
