import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import Lottie from "lottie-react";
import bg_login from "../../assets/bg_login.json";
import { NavLink } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setRegisterActionService } from "redux/actions/userAction";
import { UserOutlined } from "@ant-design/icons";
import Header from "components/Header/Header";
import styles_register from "./registerPage.module.css";

export default function RegisterPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();

  const onFinish = (values) => {
    let handleSuccess = () => {
      message.success("Đăng ký thành công !");
    };
    dispatch(setRegisterActionService(values, handleSuccess));
    setTimeout(() => {
      navigate("/login");
    }, 500);
  };
  const onFinishFailed = (errorInfo) => {};
  return (
    <div>
      <Header />
      <div
        id={styles_register.container_registerPage}
        className=" w-full h-full flex items-center justify-center"
      >
        <div
          id={styles_register.register_content}
          className="flex justify-center items-center"
        >
          {/* animate */}
          <div
            id={styles_register.register_left}
            className="flex items-center rounded-tl-lg rounded-bl-lg"
          >
            <div className="w-full ">
              <Lottie animationData={bg_login} />
            </div>
          </div>
          {/* form */}
          <div
            id={styles_register.register_right}
            className="rounded-tr-lg rounded-br-lg"
          >
            <UserOutlined
              style={{
                color: "#fff",
                display: "inline-block",
                width: "50px",
                height: "50px",
                borderRadius: "50%",
                backgroundColor: "#fb4226",
                lineHeight: "35px",
                textAlign: "top",
                fontSize: "40px",
              }}
            />
            <h3 className="text-3xl font-medium mb-3 mt-2 text-center">
              Đăng Ký
            </h3>
            <div className="w-full flex items-center justify-center">
              <Form
                className="w-full text-left pb-0"
                layout="vertical"
                name="basic"
                labelCol={{
                  span: 8,
                }}
                wrapperCol={{
                  span: 24,
                }}
                initialValues={{
                  remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
              >
                <Form.Item
                  label="Tài Khoản"
                  name="taiKhoan"
                  rules={[
                    {
                      required: true,
                      message: "Trường này không được để trống !",
                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Mật khẩu"
                  name="matKhau"
                  rules={[
                    {
                      required: true,
                      message: "Trường này không được để trống !",
                    },
                  ]}
                >
                  <Input.Password />
                </Form.Item>
                <Form.Item
                  label="Email"
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: "Trường này không được để trống !",
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Số điện thoại"
                  name="soDt"
                  rules={[
                    {
                      required: true,
                      message: "Trường này không được để trống !",
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Họ tên"
                  name="hoTen"
                  rules={[
                    {
                      required: true,
                      message: "Trường này không được để trống !",
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item className="text-center mt-3">
                  <Button
                    className=" mb-3 bg-gray-900 text-white border-none w-full"
                    htmlType="submit"
                  >
                    Đăng ký
                  </Button>
                  <NavLink
                    to="/login"
                    className="block text-right text-blue-900 text-base cursor-pointer font-semibold underline"
                    id={styles_register.link_login}
                  >
                    Bạn đã có tài khoản? Đăng nhập
                  </NavLink>
                </Form.Item>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
