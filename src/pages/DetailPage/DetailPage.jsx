import Header from "components/Header/Header";
import "./detailPage.css";
import Footer from "pages/HomePage/Footer/Footer";
import React, { useEffect, useState } from "react";
import "../../style/circle.css";
import { Tabs } from "antd";
import TabPane from "antd/lib/tabs/TabPane";

import { movieService } from "./../../service/movie.service";
import { NavLink, useParams } from "react-router-dom";
import { Rate } from "antd";
import moment from "moment";
import { useSelector } from "react-redux";
import Swal from "sweetalert2";

export default function DetailPage() {
  let params = useParams();
  let { id } = params;
  const [dataDetail, setdataDetail] = useState([]);
  useEffect(() => {
    movieService
      .getMoviesSchedule({ MaPhim: id })
      .then((res) => {
        setdataDetail(res.data.content);
      })
      .catch((err) => {});
  }, []);
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  const { hinhAnh, ngayGioChieu, tenPhim, moTa, danhGia } = dataDetail;

  return (
    <div>
      <Header />
      <div
        className={`styleall`}
        style={{
          backgroundImage: `url(${hinhAnh})`,
          height: "100vh",
          width: "100wh",
        }}
      >
        <div className={` style `}>
          <div
            style={{ paddingTop: 150 }}
            className=" grid grid-cols-12 mt-16 "
          >
            <div className="col-span-7 col-start-2">
              <div className="grid grid-cols-6">
                <img
                  style={{
                    width: "100%",
                    height: 200,
                  }}
                  className="col-span-2 object-cover"
                  src={hinhAnh}
                  alt=""
                />
                <div className="col-span-4 text-left ml-3 text-white">
                  <p className="text-sm">
                    Ngày chiếu :
                    {moment(ngayGioChieu).format("dd/mm/yyyy ~ HH:MM A")}
                  </p>
                  <p className="text-4xl mb-2">{tenPhim}</p>
                  <p className="mb-3">{moTa}</p>
                  <span className="text-white px-3 py-1 mt-3 rounded bg-orange-600">
                    <a href="#lichchieu">Đặt vé</a>
                  </span>
                </div>
              </div>
            </div>
            <div className="col-span-5 col-start-9">
              <p
                style={{ marginLeft: "34%", fontWeight: "bold" }}
                className="text-xl text-yellow-400 text-left"
              >
                Đánh giá
              </p>
              <h1
                style={{ marginLeft: "30%", marginBottom: 10 }}
                className="text-2xl text-yellow-400 text-left"
              >
                <Rate allowHalf value={danhGia / 2} />
              </h1>
              <div
                className={`c100 p${danhGia * 10} green`}
                style={{ marginLeft: "30%" }}
              >
                <span>{danhGia * 10} %</span>
                <div className="slice">
                  <div className="bar" />
                  <div className="fill" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        id="lichchieu"
        className="mt-10 ml-60 w-2/3 container bg-orange-200 px-5 py-5 mb-5"
      >
        <div>
          <h1 className="text-center text-orange-700 text-2xl">
            Lịch chiếu phim
          </h1>
          <Tabs tabPosition={"left"}>
            {dataDetail.heThongRapChieu?.map((heThongRap, index) => {
              return (
                <TabPane
                  tab={
                    <div>
                      <img src={heThongRap.logo} alt="" style={{ width: 50 }} />
                    </div>
                  }
                  key={index}
                >
                  {heThongRap.cumRapChieu?.map((cumRap, index) => {
                    return (
                      <div key={index}>
                        <div className="flex flex-row">
                          <img
                            style={{ width: 50 }}
                            src={cumRap.hinhAnh}
                            alt=""
                          />
                          <div className="text-left">
                            <p
                              style={{ fontSize: 20, fontWeight: "bold" }}
                              className="ml-2 text-xl"
                            >
                              {cumRap.tenCumRap}
                            </p>
                            <p className="ml-2 text-xxl">{cumRap.diaChi}</p>
                          </div>
                        </div>
                        <div className="grid grid-cols-4">
                          {cumRap.lichChieuPhim?.map((lichChieu, index) => {
                            return (
                              <NavLink
                                to={
                                  userInfor
                                    ? `/booktickets/${lichChieu.maLichChieu}`
                                    : Swal.fire({
                                        title: "Bạn cần đăng nhập!",
                                        icon: "warning",
                                        showCancelButton: true,
                                        allowEscapeKey: true,
                                        showCloseButton: true,
                                        confirmButtonColor: "#3085d6",
                                        cancelButtonColor: "#d33",
                                        confirmButtonText:
                                          '<a href="/login">Đăng nhập</a>',
                                      }).then((result) => {
                                        if (result.isConfirmed) {
                                          Swal.fire(
                                            "Done",
                                            "Tới trang đăng nhập nào ",
                                            "success"
                                          );
                                        }
                                      })
                                }
                                key={index}
                                className="col-span-1 bg-orange-600 ml-2 mt-2 rounded text-white text-sm"
                              >
                                {moment(lichChieu.ngayChieuGioChieu).format(
                                  "dd/mm/yyyy ~ HH:MM A"
                                )}
                              </NavLink>
                            );
                          })}
                        </div>
                      </div>
                    );
                  })}
                </TabPane>
              );
            })}
          </Tabs>
        </div>
      </div>

      <Footer />
    </div>
  );
}
