import React, { useEffect } from "react";
import { Button, Form, Input, message } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { NavLink, useNavigate } from "react-router-dom";
import { userInforLocal } from "./../../service/local.service";
import Lottie from "lottie-react";
import bg_login from "../../assets/bg_login.json";
import { useDispatch } from "react-redux";
import { setLoginActionService } from "./../../redux/actions/userAction";
import styles_login from "./loginPage.module.css";
import Header from "components/Header/Header";

export default function LoginPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  useEffect(() => {
    //Kiểm tra nếu user đã đăng nhập thì chuyển hướng về trang chủ
    let userInFor = userInforLocal.get();
    if (userInFor) {
      navigate("/");
    }
  }, []);

  const onFinish = (values) => {
    let handleSuccess = () => {
      message.success("Đăng nhập thành công !");
      setTimeout(() => {
        navigate("/");
      }, 500);
    };
    dispatch(setLoginActionService(values, handleSuccess));
  };
  const onFinishFailed = (errorInfo) => {};
  return (
    <div>
      <Header />
      <div
        id={styles_login.loginPage}
        className="h-screen w-screen flex justify-center items-center"
      >
        <div
          id={styles_login.container_login}
          className="container mx-auto flex items-center"
        >
          {/* animate */}
          <div
            id={styles_login.login_left}
            className="rounded-tl-lg rounded-bl-lg flex items-center"
          >
            <div className="w-full">
              <Lottie animationData={bg_login} />
            </div>
          </div>

          {/* form */}
          <div
            id={styles_login.login_right}
            className="rounded-tr-lg rounded-br-lg

"
          >
            <div className={styles_login.container_form}>
              <UserOutlined
                style={{
                  color: "#fff",
                  display: "inline-block",
                  width: "50px",
                  height: "50px",
                  borderRadius: "50%",
                  backgroundColor: "#fb4226",
                  lineHeight: "35px",
                  textAlign: "top",
                  fontSize: "40px",
                }}
              />
              <h3 className="text-3xl font-medium mb-3 mt-2">Đăng Nhập</h3>
              <Form
                className="w-full"
                layout="vertical"
                name="basic"
                labelCol={{
                  span: 8,
                }}
                wrapperCol={{
                  span: 24,
                }}
                initialValues={{
                  remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
              >
                <Form.Item
                  className="text-left"
                  label="Tài Khoản"
                  name="taiKhoan"
                  rules={[
                    {
                      required: true,
                      message: "Trường này không được để trống !",
                    },
                  ]}
                >
                  <Input className={styles_login.login_tk} />
                </Form.Item>

                <Form.Item
                  className="text-left"
                  label="Mật Khẩu"
                  name="matKhau"
                  rules={[
                    {
                      required: true,
                      message: "Trường này không được để trống !",
                    },
                  ]}
                >
                  <Input.Password />
                </Form.Item>

                <Form.Item>
                  <Button
                    className=" mb-4 mt-5 text-white border-none"
                    htmlType="submit"
                  >
                    ĐĂNG NHẬP
                  </Button>
                  <div className="text-right text-blue-900">
                    <NavLink
                      to="/register"
                      className="text-base cursor-pointer font-semibold underline"
                      id={styles_login.link_register}
                    >
                      Bạn chưa có tài khoản? Đăng ký
                    </NavLink>
                  </div>
                </Form.Item>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
