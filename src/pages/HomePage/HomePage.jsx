import React from "react";
import HomeCarousel from "./Carousel/Carousel";
import Header from "../../components/Header/Header";
import MovieList from "./MovieList/MovieList";
import MoviesTabs from "./MoviesTabs/MoviesTabs";
import Footer from "./Footer/Footer";

export default function HomePage() {
  return (
    <>
      <Header />
      <HomeCarousel />
      <MovieList />
      <MoviesTabs />
      <Footer />
    </>
  );
}
