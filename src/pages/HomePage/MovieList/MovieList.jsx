import React, { useEffect, useState } from "react";
import MovieItem from "./MovieItem";
import Slider from "react-slick";
import { movieService } from "./../../../service/movie.service";

const settings = {
  dots: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 2000,
  autoplaySpeed: 2000,
  cssEase: "linear",
};

const MovieList = () => {
  const [movieData, setMovieData] = useState({});
  const fetchMovies = async ({ page = 1, pageSize = 12 }) => {
    const params = {
      maNhom: "GP01",
      soTrang: page,
      soPhanTuTrenTrang: pageSize,
    };
    try {
      const res = await movieService.getMovies(params);
      setMovieData(res.data.content);
    } catch (error) {}
  };
  useEffect(() => {
    fetchMovies({});
  }, []);

  return (
    <div id="lichchieu" className="container mx-auto mt-10 mb-10">
      <Slider {...settings}>
        {movieData.items?.map((item) => (
          <MovieItem key={item.maPhim} item={item} />
        ))}
        </Slider>
      </div>
  );
};

export default MovieList;
