import React from "react";
import { NavLink } from "react-router-dom";
import "./Flip.css";

const MovieItem = (props) => {
  const { maPhim, hinhAnh, tenPhim, moTa, trailer } = props.item;
  return (
    <div>
      <div>
        <div className="flip-card mt-2">
          <div className="flip-card-inner ">
            <div className="flip-card-front rounded">
              <img
                src={hinhAnh}
                alt="Avatar"
                style={{ width: "100%", height: "100%" }}
                className="w-full h-60 object-cover rounded"
              />
            </div>
            <div className="flip-card-back px-3  rounded">
              <h2 className="text-white text-left text-xl h-16 font-bold mt-2 capitalize">
                {tenPhim}
              </h2>
              <p style={{ color: "#757582" }} className="h-16 text-left">
                {moTa.substr(0, 60) + "..."}
              </p>
              <div className="mt-3 cursor-pointer ">{/* modal here */}</div>
            </div>
          </div>
          <div
            style={{ backgroundColor: "#f49b4b" }}
            className="py-2 my-2 rounded text-xl text-white cursor-pointer font-bold"
          >
            <NavLink className="block" to={`/detail/${maPhim}`}>
              Đặt vé
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MovieItem;
{
  /* <NavLink
className=" btn_buy py-2 my-2 rounded bg-orange-700 text-lg text-white cursor-pointer font-medium block"
to={`/detail/${maPhim}`}
>
ĐẶT VÉ
</NavLink> */
}
