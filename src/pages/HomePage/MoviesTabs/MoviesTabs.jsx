import React, { useEffect, useState } from "react";
import { movieService } from "./../../../service/movie.service";
import { Tabs } from "antd";
import TabPane from "antd/lib/tabs/TabPane";
import MovieTabItem from "./MovieTabItem";
export default function MoviesTabs() {
  const [dataRaw, setdataRaw] = useState([]);

  useEffect(() => {
    movieService
      .getMoviesByTheater()
      .then((res) => {
        setdataRaw(res.data.content);
      })
      .catch((err) => {});
  }, []);
  let renderHeThongRap = () => {
    return dataRaw.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={
            <div id="cumrap" className="border-b-2 my-0 p-2">
              <img className="w-20 h-20 " src={heThongRap.logo} />
            </div>
          }
          key={index}
        >
          <Tabs tabPosition="left" defaultActiveKey="1">
            {heThongRap.lstCumRap.map((cumRap) => {
              return (
                <TabPane
                  className=" w-full overflow-y-scroll"
                  tab={
                    <div id="cumrap" className="w-60 h-24 border-b-2 text-left">
                      <h2 className=" text-orange-600 font-bold uppercase">
                        {cumRap.tenCumRap}
                      </h2>
                      <p className="text-black">
                        {cumRap.diaChi.substr(0, 30) + "..."}
                      </p>
                    </div>
                  }
                  key={cumRap.maCumRap}
                >
                  {cumRap.danhSachPhim.map((phim) => {
                    return <MovieTabItem movie={phim} />;
                  })}
                </TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };
  return (
    <div className=" container flex justify-center mx-auto lg:px-10 ">
      <Tabs
        tabPosition="left"
        defaultActiveKey="1"
        className="lg:w-full xl:w-5/6 border-4 rounded-lg"
      >
        {renderHeThongRap()}
      </Tabs>
    </div>
  );
}
