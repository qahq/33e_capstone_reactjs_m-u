import moment from "moment";
import React from "react";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import Swal from "sweetalert2";

export default function MovieTabItem({ movie }) {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });

  return (
    <div className="flex p-3 items-center">
      <img className="h-24 w-24 object-cover" src={movie.hinhAnh} alt="img" />
      <div className="ml-5">
        <p className="text-xl text-left font-medium">{movie.tenPhim}</p>
        <div className="grid grid-cols-3 gap-2">
          {movie.lstLichChieuTheoPhim.slice(0, 6).map((item, index) => {
            return (
              <NavLink
                to={userInfor ? `/booktickets/${item.maLichChieu}` : `/login`}
                key={index}
                className="p-3 bg-orange-600 rounded text-white text-sm "
              >
                {moment(item.ngayChieuGioChieu).format("dd/mm/yyyy ~ HH:MM A")}
              </NavLink>
            );
          })}
        </div>
      </div>
    </div>
  );
}
