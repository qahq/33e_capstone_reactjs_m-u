import React, { useEffect, useState } from "react";
import { movieService } from "../../../service/movie.service";
import Carousel from "react-bootstrap/Carousel";
import styles_carousel from "./carousel.module.css";

export default function HomeCarousel() {
  const [banner, setBanner] = useState([]);
  useEffect(() => {
    movieService
      .getBanner()
      .then((res) => {
        setBanner(res.data.content);
      })
      .catch((err) => {});
  }, []);

  let renderCarousel = () => {
    return banner.map((item, index) => {
      return (
        <Carousel.Item key={index} className="w-full h-100 mx-auto">
        <img
          className="d-block w-full"
          src={item.hinhAnh}
          alt="img"
        />
      </Carousel.Item>
      );
    });
  };

  return (
    <Carousel className={styles_carousel.container_carousel}>
      {renderCarousel()}
    </Carousel>
  );
}
