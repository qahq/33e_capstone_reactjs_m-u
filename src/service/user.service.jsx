import { https } from "./url.config";

export const userServ = {
  postLogin: (dataLogin) => {
    let uri = "/api/QuanLyNguoiDung/DangNhap";
    return https.post(uri, dataLogin);
  },
  postRegister: (dataRegister) => {
    let uri = "/api/QuanLyNguoiDung/DangKy";
    return https.post(uri, dataRegister);
  },
  postThongTinNguoiDung: () => {
    let uri = "/api/QuanLyNguoiDung/ThongTinTaiKhoan";
    return https.post(uri);
  },
};
