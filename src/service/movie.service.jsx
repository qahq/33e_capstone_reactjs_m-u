import { https } from "./url.config";
import { ThongTinDatVe } from "./../models/thongTinDatVe";

export const movieService = {
  getBanner: () => {
    return https.get("api/QuanLyPhim/LayDanhSachBanner");
  },
  getMovies: (params) => {
    let uri = "api/QuanLyPhim/LayDanhSachPhimPhanTrang";
    return https.get(uri, { params: params });
  },
  getMoviesByTheater: () => {
    let uri = "api/QuanLyRap/LayThongTinLichChieuHeThongRap";
    return https.get(uri);
  },
  getMoviesSchedule: (params) => {
    let uri = `/api/QuanLyRap/LayThongTinLichChieuPhim`;
    return https.get(uri, { params });
  },
  getListTicketRoom: (params) => {
    let uri = `/api/QuanLyDatVe/LayDanhSachPhongVe`;
    return https.get(uri, { params });
  },
  setTickets: (thongTinDatVe = new ThongTinDatVe()) => {
    let uri = "/api/QuanLyDatVe/DatVe";
    return https.post(uri, thongTinDatVe);
  },
};
