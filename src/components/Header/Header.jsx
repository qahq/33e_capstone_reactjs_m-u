import React from "react";
import UserNav from "./UserNav";
import { NavLink } from "react-router-dom";
import styles_header from "./header.module.css";

export default function Header() {
  return (
    <div className={styles_header.header}>
      <div className="mx-auto flex justify-between items-center h-20 bg-black w-full text-white z-20 pr-16 pl-16 top-0 fixed">
        <NavLink to="/">
          <img
            src="https://cyber-movie-bootstrap.vercel.app/img/logo.svg"
            alt="logo"
            width={170}
          />
        </NavLink>
        <div className="font-medium">
          <a className={styles_header.link} href="#lichchieu">
            <span className="text-lg">Lịch chiếu</span>
          </a>
          <a className={styles_header.link} href="#cumrap">
            <span className="mx-16 text-lg">Cụm rạp</span>
          </a>
          <a className={styles_header.link} href="#">
            <span className="text-lg">Tin tức</span>
          </a>
        </div>
        <UserNav />
      </div>
    </div>
  );
}
