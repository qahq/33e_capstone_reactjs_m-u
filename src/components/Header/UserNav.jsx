import React from "react";
import { useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { userInforLocal } from "../../service/local.service";
import { useDispatch } from "react-redux";
import { SET_LOGIN } from "../../redux/constants/userConstants";
import { LogoutOutlined, UserOutlined } from "@ant-design/icons";
import styles_nav from "./header.module.css";
import Swal from "sweetalert2";

export default function UserNav() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  let handleLogOut = () => {
    Swal.fire({
      title: "Bạn chắc chắn muốn đăng xuất!",
      icon: "warning",
      showCancelButton: true,
      allowEscapeKey: true,
      showCloseButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Đăng Xuất",
    }).then((result) => {
      if (result.isConfirmed) {
        userInforLocal.remove();
        dispatch({
          type: SET_LOGIN,
          payload: null,
        });
        Swal.fire(
          "Đã đăng xuất",
          "Đăng xuất thành công! " + "Cảm ơn bạn đã sử dụng ứng dụng",
          "success"
        );
      }
    });
    setTimeout(() => {
      navigate("/");
    }, 500);
  };
  let renderContent = () => {
    if (userInfor) {
      return (
        <div className="space-x-4 font-medium">
          <button
            id={styles_nav.nav_user_info}
            className="text-lg text-white px-4 py-1 rounded"
          >
            <i class="fa fa-user-astronaut mr-2" />
            {userInfor.hoTen}
          </button>
          <button
            id={styles_nav.btn_logout}
            onClick={handleLogOut}
            className=" text-lg text-white px-4 py-1 rounded  bg-black cursor-pointer "
          >
            <i class="fa fa-sign-in-alt mr-2" />
            Đăng Xuất
          </button>
        </div>
      );
    } else {
      return (
        <div className="space-x-4 font-medium">
          <NavLink
            to="/login"
            className=" text-lg text-white px-5 py-2 rounded"
            id={styles_nav.btn_login}
          >
            Đăng Nhập
          </NavLink>
          <NavLink
            to="/register"
            className="text-lg text-white  px-5 py-2 rounded"
            id={styles_nav.btn_logout}
          >
            Đăng Ký
          </NavLink>
        </div>
      );
    }
  };

  return <div>{renderContent()}</div>;
}
