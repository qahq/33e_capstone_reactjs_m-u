import React from "react";
import { CircleLoader } from "react-spinners";
import { useSelector } from "react-redux";
import Lottie from "lottie-react";
import loading from "../../assets/loding.json"

export default function Spinner() {
  let { isLoading } = useSelector((state) => {
    return state.spinnerReducer;
  });
  return isLoading ? (
    <div className="fixed w-screen h-screen flex justify-center items-center bg-white top-0 left-0 z-50">
        <Lottie animationData={loading} />
    </div>
  ) : (
    <></>
  );
}
