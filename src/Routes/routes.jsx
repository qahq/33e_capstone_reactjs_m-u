import HomePage from "pages/HomePage/HomePage";
import LoginPage from "./../pages/LoginPage/LoginPage";
import DetailPage from "./../pages/DetailPage/DetailPage";
import RegisterPage from "pages/RegisterPage/RegisterPage";
import BookticketPage from "./../pages/BookticketPage/BookticketPage";

export const routes = [
  {
    path: "/",
    component: <HomePage />,
  },
  {
    path: "/login",
    component: <LoginPage />,
  },
  {
    path: "/register",
    component: <RegisterPage />,
  },
  {
    path: "/detail/:id",
    component: <DetailPage />,
  },
  {
    path: "/booktickets/:id",
    component: <BookticketPage />,
  },
];
